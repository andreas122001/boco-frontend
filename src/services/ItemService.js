import axios from 'axios'

const CONFIG = {
    baseURL: process.env.VUE_APP_API_ENDPOINT + '/items',
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    }
}

export async function getItem(id) {
    try {
        const response = await axios.get(`/${id}`, CONFIG)
        return response.data
    } catch (e) {
        throw new Error('An unexpected error occured. Could not load item.')
    }
}

export async function getItems({ limit, page, sort, sortDir, category, search, email }) {
    try {
        const params = new URLSearchParams()
        if (limit) params.append('limit', limit)
        if (page) params.append('page', page)
        if (sort) params.append('sort', sort)
        if (sortDir) params.append('sortDir', sortDir)
        if (category) params.append('category', category)
        if (search) params.append('search', search)
        if (email) params.append('email', email)

        const paramsStr = params.toString()
        const response = await axios.get('?' + paramsStr, CONFIG)
        return response.data
    } catch (e) {
        console.error('Unable to get items', e)
        throw new Error('An unexpected error occurred. Could not load items.')
    }
}

export class NoItemsFoundError extends Error {}

export async function getAllItemsBySearch(searchValue) {
    try {
        const response = await axios.get(`/search/${searchValue}`, CONFIG)
        return response.data
    } catch (e) {
        if (e.response && e.response.status === 304) {
            throw new NoItemsFoundError()
        }
        console.error(e)
        throw new Error('Error searching for item. Try again.')
    }
}

export async function getUserItems(limit, page, userEmail) {
    try {
        const response = await axios.get(`?limit=${limit}&page=${page}&email=${encodeURIComponent(userEmail)}`, CONFIG)
        return response.data
    } catch (e) {
        console.error('Could not load items', e)
        throw new Error('An unexpected error occurred. Could not load your items.')
    }
}

export async function createItem(itemInfo) {
    try {
        const response = await axios.post('/', itemInfo, CONFIG)
        return response.data
    } catch (e) {
        console.error('Could not create item', e)
        throw new Error('An unexpected error occured. Could not create item.')
    }
}

export async function updateItem(itemInfo, id) {
    try {
        const response = await axios.put(`/${id}`, itemInfo, CONFIG)
        return response.data
    } catch (e) {
        console.error('Could not update item.', e)
        throw new Error('An unexpected error occured. Could not update item.')
    }
}

export async function addImageToItem(pictureInfo) {
    try {
        const response = await axios.post(`/${pictureInfo.itemId}/images`, pictureInfo, CONFIG)
        return response.data
    } catch (e) {
        console.error('Image could not be added', e)
        throw new Error('An unexpected error occured. Image could not be stored and added to item.')
    }
}

export async function getItemImages(itemId) {
    try {
        const response = await axios.get(`/${itemId}/images`, CONFIG)
        return response.data
    } catch (e) {
        console.error('Item images could not be loaded', e)
        throw new Error('An unexpected error occurred. Images to the item could not be loaded.')
    }
}

export async function getItemImage(itemId, imageId) {
    try {
        const response = await axios.get(`/${itemId}/images/${imageId}`, CONFIG)
        return response.data
    } catch (e) {
        console.error('Item image could not be loaded')
        throw new Error(`An unexpected error occurred. Image ${imageId} to item ${itemId} could not be loaded.`)
    }
}

export async function deleteItem(id) {
    try {
        const response = await axios.delete(`/${id}`, CONFIG)
        return response.data
    } catch (e) {
        console.error('Item could not be deleted', e)
        throw new Error('An unexpected error occured. Item could not be deleted.')
    }
}

export async function deleteImageToItem(itemId, imageId) {
    try {
        const response = await axios.delete(`/${itemId}/images/${imageId}`, CONFIG)
        return response.data
    } catch (e) {
        console.error('Image to item could not be deleted', e)
        throw new Error('An unexpected error occured. Image to item could not be deleted.')
    }
}

export async function getPriceUnits() {
    try {
        const response = await axios.get('/priceUnits', CONFIG)
        return response.data
    } catch (e) {
        console.error('Could not get price units', e)
        throw new Error('An unexpected error occured. Could not get price units.')
    }
}

export async function getCategories() {
    try {
        const response = await axios.get('/categories', CONFIG)
        return response.data
    } catch (e) {
        console.error('Could not get categories', e)
        throw new Error('An unexpected error occured. Could not get categories.')
    }
}

export async function getLeasedItemDateFrames(itemId) {
    try {
        const response = await axios.get(`${itemId}/leases`, CONFIG)
        return response.data
    } catch (e) {
        console.error('Could not get leased timeframes', e)
        throw new Error('An unexpected error occured. Could not get leased timeframes')
    }
}
