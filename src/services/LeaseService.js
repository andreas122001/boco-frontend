import axios from 'axios'

const CONFIG = {
    baseURL: process.env.VUE_APP_API_ENDPOINT + '/leases',
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    }
}

export async function createLease(leaseInfo) {
    try {
        const response = await axios.post('/', leaseInfo, CONFIG)
        return response.data
    } catch (e) {
        console.error('Could not create lease', e)
        throw new Error('An unexpected error occured. Lease could not be initialized.')
    }
}
