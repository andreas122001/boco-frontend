// import axios from 'axios'
import SockJS from 'sockjs-client'
import { Client } from '@stomp/stompjs'
import store from '@/store/index'
import axios from 'axios'

const CONFIG = {
    baseURL: process.env.VUE_APP_API_ENDPOINT + '/chats',
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    }
}

export function getChatWebSocketConnection(
    subscriptionPath,
    connectCallback,
    errorCallback,
    messageCallback
) {
    const brokerUrl = process.env.VUE_APP_API_ENDPOINT + '/ws'

    const stompClient = new Client({
        brokerUrl: brokerUrl,
        reconnectDelay: 5_000,
        connectionTimeout: 100_000,
        webSocketFactory: () => new SockJS(brokerUrl),
        connectHeaders: {
            'Session-Token': store.getters['authentication/session'].token
        }
    })

    stompClient.onConnect = () => {
        connectCallback()

        stompClient.subscribe(subscriptionPath, messageCallback)
    }
    stompClient.onWebSocketClose = errorCallback

    return stompClient
}

export async function getChats() {
    try {
        const response = await axios.get('/', CONFIG)
        return response.data
    } catch (e) {
        throw new Error('An unexpected error occured. Could not retrieve chats.')
    }
}

export async function getChat(itemId) {
    try {
        const response = await axios.get('/' + itemId, CONFIG)
        return response.data
    } catch (e) {
        throw new Error('An unexpected error occured. Could not retrieve chats.')
    }
}

export class ChatAlreadyExistsError extends Error {
    constructor(message) {
        super(message)
        this.name = 'ChatAlreadyExistsError'
    }
}

export async function createChat(chat) {
    try {
        const response = await axios.post('/', chat, CONFIG)
        return response.data
    } catch (e) {
        if (e.response && e.response.status === 409) {
            throw new ChatAlreadyExistsError('There already exists a chat between these users for this item')
        }
        throw new Error('An unexpected error occured. Could not create a new chat.')
    }
}

export async function getMessagesForChat(chatUUID) {
    try {
        const response = await axios.get(`/${chatUUID}/messages`, CONFIG)
        return response.data
    } catch (e) {
        throw new Error('An unexpected error occured. Could not retrieve messages for this chat.')
    }
}

export async function sendLeaseNotice(notice, chatUUID) {
    try {
        const response = await axios.post(`/${chatUUID}`, notice, CONFIG)
        return response.data
    } catch (e) {
        throw new Error('An unexpected error occured. Could not send lease notice messages for this chat.')
    }
}
