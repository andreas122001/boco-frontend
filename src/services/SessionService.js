import axios from 'axios'

const CONFIG = {
    baseURL: process.env.VUE_APP_API_ENDPOINT + '/sessions',
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    }
}

export class InvalidCredentialsError extends Error {
    constructor(message, options) {
        if (!message) {
            message = 'The username or password was invalid'
        }
        super(message, options)
    }
}

export async function createSession(userCredentials) {
    try {
        const response = await axios.post('', userCredentials, CONFIG)
        return { token: response.data.token }
    } catch (e) {
        if (e.response && e.response.status === 401) {
            throw new InvalidCredentialsError()
        }
        console.error('Unable to create session', e)
        throw new Error('An unexpected error occured.')
    }
}

export async function deleteSession(session) {
    localStorage.clear()
    const token = encodeURIComponent(session.token)
    try {
        await axios.delete(`/${token}`, CONFIG)
    } catch (e) {
        console.error('Unable to delete session', e)
        throw new Error('An unexpected error occured.')
    }
}
