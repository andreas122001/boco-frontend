import axios from 'axios'

const CONFIG = {
    baseURL: process.env.VUE_APP_API_ENDPOINT + '/users',
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    }
}

export async function changePassword(currentPassword, newPassword) {
    try {
        const response = await axios.put('/self/password', { oldPassword: currentPassword, newPassword: newPassword }, CONFIG)
        return response.data
    } catch (e) {
        let err

        if (e.response && e.response.status === 400) {
            err = 'New password cannot be the same as the old password'
        } else if (e.response && e.response.status === 401) {
            err = 'Invalid current password'
        } else {
            err = 'An unexpected error occured.'
        }

        throw new Error(err)
    }
}

export async function register(userInfo) {
    try {
        const response = await axios.post('', userInfo, CONFIG)
        return response.data
    } catch (e) {
        if (e.response && e.response.status === 409) {
            throw new Error('A user with this e-mail already exists')
        }
        console.error('Unable to register user', e)
        throw new Error('An unexpected error occured.')
    }
}

export async function getUserdata() {
    try {
        const response = await axios.get('/self', CONFIG)
        return response.data
    } catch (e) {
        console.error('Unable to retrieve user', e)
        throw new Error('An unexpected error occured. Could not retrieve user profile.')
    }
}

export async function getUserdataByEmail(email) {
    try {
        const response = await axios.get('/' + email, CONFIG)
        return response.data
    } catch (e) {
        console.error('Unable to retrieve user', e)
        throw new Error('An unexpected error occured. Failed to retrieve user data.')
    }
}

export async function getUserdataById(id) {
    try {
        const response = await axios.get('/id/' + id, CONFIG)
        return response.data
    } catch (e) {
        console.error('Unable to retrieve user', e)
        throw new Error('An unexpected error occurred. Failed to retrieve user data.')
    }
}

export async function setUserdata(userData) {
    try {
        const response = await axios.put('/self', userData, CONFIG)
        return response.data
    } catch (e) {
        console.error('Could not update user', e)
        throw new Error('An unexpected error occured. Could not update user profile.')
    }
}

export async function deleteUser() {
    try {
        const response = await axios.delete('/self', CONFIG)
        return response.data
    } catch (e) {
        console.error('Unable to delete user')
        throw new Error('An unexpected error occured. Could not delete user profile.')
    }
}
