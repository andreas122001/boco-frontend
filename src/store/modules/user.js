import { getUserdata } from '@/services/UserService'

export default {
    namespaced: true,
    state: {
        user: {
            id: '',
            firstName: '',
            lastName: '',
            email: '',
            address: {
                street: '',
                postalCode: ''
            }
        }
    },
    getters: {
        user(state) {
            return state.user
        },
        firstName(state) {
            return state.user.firstName
        },
        lastName(state) {
            return state.user.lastName
        },
        email(state) {
            return state.user.email
        },
        address(state) {
            return state.user.address
        },
        street(state) {
            return state.user.address.street
        },
        postalCode(state) {
            return state.user.address.postalCode
        }
    },
    actions: {
        async retrieveData(context) {
            try {
                const user = await getUserdata()
                context.commit('SET_USER', user)
            } catch (e) {
                console.error('unable to retrieve user', e)
            }
        },
        async setUser(context, user) {
            try {
                context.commit('SET_USER', user)
            } catch (e) {
                console.error('unable to set user', e)
            }
        }
    },
    mutations: {
        SET_USER(state, user) {
            state.user = user
        },
        CLEAR(state) {
            state.user = {}
        }
    }
}
