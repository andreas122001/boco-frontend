
export default {
    namespaced: true,
    state: {
        redirectGoal: {},
        route: {}
    },
    getters: {
        redirectGoal(state) {
            return state.redirectGoal
        },
        route(state) {
            return state.route
        }
    },
    actions: {
        setRedirectGoal(context, redirectGoal) {
            context.commit('SET_REDIRECT_GOAL', redirectGoal)
        },
        findRoute(context, redirectGoal) {
            context.commit('FIND_ROUTE', redirectGoal)
        }
    },
    mutations: {
        SET_REDIRECT_GOAL(state, redirectGoal) {
            state.redirectGoal = redirectGoal
        },
        FIND_ROUTE(state, redirectGoal) {
            state.route = (redirectGoal && redirectGoal !== '')
                ? redirectGoal
                : { path: '/' }
        }
    }
}
