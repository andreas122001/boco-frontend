import { getAllItems } from '@/services/ItemService'

export default {
    state: () => ({
        items: null,
        item: null,
        address: ''
    }),

    mutations: {
        ALL_ITEMS(state) {
            getAllItems().then((response) => {
                state.this.items = response.data
            })
        }
    },

    actions: {
    },
    getters: {
    }
}
