import axios from 'axios'
import { createSession, deleteSession } from '@/services/SessionService'
import router from '@/router/index.js'
import store from '@/store'

export default {
    namespaced: true,
    state: {
        session: null
    },
    getters: {
        session(state) {
            return state.session
        }
    },
    actions: {
        async login(context, userCredentials) {
            const session = await createSession(userCredentials)
            localStorage.setItem('token', session.token)
            context.dispatch('setSession', session)
        },

        async logout(state, context) {
            if (state.session === null) return

            try {
                await deleteSession(state.session)
                delete axios.defaults.headers.common.Authorization
                localStorage.removeItem('token')
                context.commit('LOGOUT_SUCCESS')
            } catch (e) {
                console.error('unable to log out', e)
            }
        },
        stopSession(context) {
            store.commit('user/CLEAR')
            delete axios.defaults.headers.common.Authorization
            localStorage.clear()
            context.commit('LOGOUT_SUCCESS')
        },
        setSession(context, session) {
            context.commit('LOGIN_SUCCESS', session)
            axios.defaults.headers.common.Authorization = `Bearer ${session.token}`
        }
    },
    mutations: {
        LOGIN_SUCCESS(state, session) {
            state.session = session
        },
        LOGOUT_SUCCESS(state) {
            state.session = null
            router.push('/')
        }
    }
}
