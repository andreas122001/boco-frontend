import { createStore } from 'vuex'
import user from './modules/user'
import authentication from './modules/authentication'
import routing from './modules/routing'
import item from './modules/item'

export default createStore({
    state: {
    },
    getters: {
    },
    actions: {
    },
    mutations: {
    },
    modules: {
        user,
        authentication,
        routing,
        item
    }
})
