import { createRouter, createWebHistory } from 'vue-router'
import CreateItemView from '@/views/CreateItemView.vue'
import store from '@/store/index'
import HomeView from '@/views/HomeView.vue'
import ItemView from '@/views/ItemView.vue'
import EditItemView from '@/views/EditItemView.vue'
import MyItemsView from '@/views/MyItemsView.vue'
import LoginView from '@/views/LoginView.vue'
import RegisterView from '@/views/RegisterView.vue'
import AccountView from '@/views/AccountView.vue'
import ChangePasswordView from '@/views/ChangePasswordView.vue'
import RentItemView from '@/views/RentItemView.vue'
import UserDetailsView from '@/views/UserDetailsView'
import ChatView from '@/views/ChatView.vue'
import PrivacyPolicyView from '@/views/PrivacyPolicyView.vue'
import ForgotPasswordView from '@/views/ForgotPasswordView'

const routes = [
    {
        path: '/forgot-password',
        name: 'forgotPassword',
        component: ForgotPasswordView
    },
    {
        path: '/profile/:id',
        name: 'userDetails',
        component: UserDetailsView,
        props: true
    },
    {
        path: '/',
        name: 'home',
        component: HomeView
    },
    {
        path: '/register',
        name: 'register',
        component: RegisterView
    },
    {
        path: '/item/:id',
        name: 'item',
        props: true,
        component: ItemView
    },
    {
        path: '/edit-item',
        name: 'edit-item',
        props: true,
        component: EditItemView,
        beforeEnter: (to, from) => {
            return conditionalLoginReroute(to)
        }
    },
    {
        path: '/create-item',
        name: 'create-item',
        component: CreateItemView,
        beforeEnter: (to, from) => {
            return conditionalLoginReroute(to)
        }
    },
    {
        path: '/messages',
        name: 'messages',
        component: null,
        beforeEnter: (to, from) => {
            return conditionalLoginReroute(to)
        }
    },
    {
        path: '/my-account',
        name: 'my-account',
        component: AccountView,
        beforeEnter: (to, from) => {
            return conditionalLoginReroute({ name: 'home' })
        }
    },
    {
        path: '/faq',
        name: 'faq',
        component: null
    },
    {
        path: '/my-items',
        name: 'my-items',
        component: MyItemsView,
        beforeEnter: (to, from) => {
            return conditionalLoginReroute(to)
        }
    },
    {
        path: '/login',
        name: 'login',
        component: LoginView,
        beforeEnter: (to, from) => {
            if (store.getters['authentication/session']) {
                return { name: 'home' }
            }
        }
    },
    {
        path: '/change-password',
        name: 'change-password',
        component: ChangePasswordView,
        beforeEnter: (to, from) => {
            return conditionalLoginReroute(to)
        }
    },
    {
        path: '/rent-item',
        name: 'rent-item',
        props: true,
        component: RentItemView,
        beforeEnter: (to, from) => {
            return conditionalLoginReroute(from)
        }
    },
    {
        path: '/messages',
        name: 'messages',
        props: true,
        component: ChatView,
        beforeEnter: (to, from) => {
            return conditionalLoginReroute(to)
        }
    },
    {
        path: '/privacy-policy',
        name: 'privacy-policy',
        component: PrivacyPolicyView
    }
]

const conditionalLoginReroute = async(redirectGoal) => {
    if (!await store.getters['authentication/session']) {
        await store.dispatch('routing/setRedirectGoal', redirectGoal)
        return { path: '/login' }
    }
}

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router
