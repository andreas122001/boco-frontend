export class MapSearchOption {
    static ADDRESS = new MapSearchOption('Address');
    static POSTALAREA = new MapSearchOption('Postal area');

    constructor(name) {
        this.name = name
    }

    toString() {
        return `${this.name}`
    }
}
