/**
 * Debouncer facilitates debouncing of inputs.
 */
export class Debouncer {
    /**
     * Creates a new debouncer.
     * @param {number} milliseconds Amount of milliseconds to debounce.
     * @param {Function} handler The handler to invoke when the debouncer ticks.
     */
    constructor(milliseconds, handler) {
        this.milliseconds = milliseconds
        this.handler = handler
        this.timeoutId = null
        this.defer = false
        this.deferred = false
    }

    /**
     * Ticks the debouncer, invoking the handler.
     * If the debouncer is set to deferred, the tick will ocurr
     * after it is reset.
     */
    tick() {
        if (this.defer) {
            this.deferred = true
            return
        }
        this.cancel()
        const value = this.handler()
        if (value instanceof Promise) {
            this.setDefer(true)
            value.finally(() => {
                this.setDefer(false)
            })
        }
    }

    setDefer(value) {
        this.defer = value
        if (!this.defer && this.deferred) {
            this.tick()
        }
    }

    debounce() {
        this.cancel()
        this.timeoutId = setTimeout(this.tick.bind(this), this.milliseconds)
    }

    cancel() {
        if (this.timeoutId !== null) {
            clearTimeout(this.timeoutId)
        }
        this.deferred = false
    }
}
