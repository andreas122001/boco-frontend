
export function translateDateToString(dateTimeData) {
    const dateTime = new Date(dateTimeData)
    const month = dateTime.getMonth() + 1
    const day = dateTime.getDate()
    const year = dateTime.getFullYear()

    return `${month}.${day}.${year}`
}
