import { shallowMount } from '@vue/test-utils'
import RegisterView from '@/views/RegisterView.vue'

describe('RegisterView', () => {
    let wrapper

    const findTestComponent = (id) => wrapper.find(`[data-testid=${id}]`)

    beforeEach(() => {
        wrapper = shallowMount(RegisterView)
    })

    it('renders inputs when loaded', () => {
        expect(findTestComponent('firstNameField').exists()).toBeTruthy()
        expect(findTestComponent('lastNameField').exists()).toBeTruthy()
        expect(findTestComponent('emailField').exists()).toBeTruthy()
        expect(findTestComponent('addressField').exists()).toBeTruthy()
        expect(findTestComponent('zipField').exists()).toBeTruthy()
        expect(findTestComponent('passwordField').exists()).toBeTruthy()
        expect(findTestComponent('repeatPasswordField').exists()).toBeTruthy()
    })
})
