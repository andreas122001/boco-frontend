import { Debouncer } from '@/util/Debouncer'
import sinon from 'sinon'

describe('Debouncer', () => {
    let setTimeoutSpy

    beforeEach(() => {
        setTimeoutSpy = sinon.spy(window, 'setTimeout')
        sinon.spy(window, 'clearTimeout')
    })

    afterEach(() => {
        sinon.restore()
    })

    it('Runs the handler instantly when ticked', () => {
        let ran = false
        const handler = () => { ran = true }
        const debouncer = new Debouncer(100, handler)

        debouncer.tick()

        expect(ran).toBe(true)
    })

    it('Does not tick when deferred', () => {
        let ran = false
        const handler = () => { ran = true }
        const debouncer = new Debouncer(100, handler)

        debouncer.setDefer(true)
        debouncer.tick()

        expect(ran).toBe(false)
    })

    it('Defers the tick until defer is disabled', () => {
        let ran = false
        const handler = () => { ran = true }
        const debouncer = new Debouncer(100, handler)

        debouncer.setDefer(true)
        debouncer.tick()
        debouncer.setDefer(false)

        expect(ran).toBe(true)
    })

    it('Sets timeout to milliseconds when debouncing', () => {
        const debouncer = new Debouncer(1337, () => {})

        debouncer.debounce()

        expect(setTimeoutSpy.getCall(0).args[1]).toBe(1337)
    })

    it('Runs the handler when the debounce times out', () => {
        let ran = false
        const handler = () => { ran = true }
        const debouncer = new Debouncer(100, handler)

        debouncer.debounce()
        setTimeoutSpy.getCall(0).args[0]()

        expect(ran).toBe(true)
    })
})
