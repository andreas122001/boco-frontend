import { createLease } from '@/services/LeaseService.js'
import axios from 'axios'

jest.mock('axios')

const MOCK_LEASE = {
    userEmail: 'custom@email.com',
    itemId: 3,
    comment: 'simple comment',
    startTime: new Date(),
    endTime: new Date()
}
describe('LeaseService', () => {
    it('successfully create lease', async() => {
        axios.post.mockImplementation(() => Promise.resolve({ data: MOCK_LEASE }))

        const itemResult = await createLease(MOCK_LEASE)

        expect(itemResult)
            .toEqual(MOCK_LEASE)
    })

    it('could not make lease, unexpected error', async() => {
        const rejectionError = new Error()
        axios.post.mockImplementation(() => Promise.reject(rejectionError))

        const expectedResponse = { message: 'An unexpected error occured. Lease could not be initialized.' }
        await expect(createLease(MOCK_LEASE))
            .rejects
            .toThrow(expectedResponse)
    })
})
