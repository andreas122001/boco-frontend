import { getItems, addImageToItem, createItem, getItem, getUserItems, getItemImages, getItemImage, deleteItem, getCategories, getPriceUnits, updateItem } from '@/services/ItemService.js'
import axios from 'axios'
import sinon from 'sinon'

const ITEM = {
    id: 1,
    title: 'Test title',
    price: 100,
    description: 'Test description',
    startTime: 'Test startTime',
    endTime: 'Test endTime',
    location: 'Test location',
    imageIds: [0, 1]
}

const ITEM_RESPONSE_DATA = {
    id: '1',
    title: 'Test title',
    price: '100',
    description: 'Test description',
    startTime: 'Test startTime',
    endTime: 'Test endTime',
    location: 'Test location',
    imageIds: '["0", "1"]'
}

const IMAGE = {
    id: 1,
    description: '',
    image: Image(),
    itemId: 3,
    altText: 'altText'
}

const IMAGE_RESPONSE_DATA = {
    id: '2',
    description: '',
    data: 'djakbdkabdabdlbasd&&DA(&d8ad203BJSKA....',
    itemId: '1',
    altText: 'altText'
}

const CATEGORY_ENUM_RESPONSE_DATA = {
    0: 'OTHER',
    1: 'SPORTS'
}

const PRICEUNIT_ENUM_RESPONSE_DATA = {
    0: 'DAY',
    1: 'HOUR'
}

describe('ItemService', () => {
    afterEach(() => {
        sinon.restore()
    })

    it('Gets an item from an id', async() => {
        const expectedItem = ITEM_RESPONSE_DATA

        const response = {
            data: ITEM_RESPONSE_DATA,
            status: 200,
            statusText: 'OK'
        }
        sinon.stub(axios, 'get').resolves(response)
        const actualItem = await getItem(ITEM.id)

        expect(actualItem).toEqual(expectedItem)
    })

    it('Throws error if item is not found', async() => {
        const response = {
            error: 'error'
        }
        sinon.stub(axios, 'get').rejects(response)

        await expect(getItem(0)).rejects.toEqual(Error('An unexpected error occured. Could not load item.'))
    })

    it('Gets all items without user account', async() => {
        const expectedItems = [ITEM_RESPONSE_DATA, ITEM_RESPONSE_DATA]

        const response = {
            data: [ITEM_RESPONSE_DATA, ITEM_RESPONSE_DATA],
            status: 200,
            statusText: 'OK'
        }
        sinon.stub(axios, 'get').resolves(response)
        const actualItems = await getItems({})

        expect(actualItems).toEqual(expectedItems)
    })

    it('Throws error if items are not found', async() => {
        const response = {
            error: 'error'
        }
        sinon.stub(axios, 'get').rejects(response)

        await expect(getItems({})).rejects.toEqual(Error('An unexpected error occurred. Could not load items.'))
    })

    it('Gets all items owned by a user', async() => {
        const expectedItems = [ITEM_RESPONSE_DATA, ITEM_RESPONSE_DATA]

        const response = {
            data: [ITEM_RESPONSE_DATA, ITEM_RESPONSE_DATA],
            status: 200,
            statusText: 'OK'
        }
        sinon.stub(axios, 'get').resolves(response)
        const userEmail = 'test@mail.com'
        const actualItems = await getUserItems(userEmail)

        expect(actualItems).toEqual(expectedItems)
    })

    it('Throws error if items are not found', async() => {
        const response = {
            error: 'error'
        }
        sinon.stub(axios, 'get').rejects(response)
        const userEmail = 'test@mail.com'

        await expect(getUserItems(userEmail)).rejects.toEqual(Error('An unexpected error occurred. Could not load your items.'))
    })

    it('Creates an item from the given item info', async() => {
        const expectedItem = ITEM_RESPONSE_DATA

        const response = {
            data: ITEM_RESPONSE_DATA,
            status: 201,
            statusText: 'Created'
        }
        sinon.stub(axios, 'post').resolves(response)
        const actualItem = await createItem(ITEM)

        expect(actualItem).toEqual(expectedItem)
    })

    it('Throws error if item could not be created', async() => {
        const response = {
            error: 'error'
        }
        sinon.stub(axios, 'post').rejects(response)

        await expect(createItem(ITEM)).rejects.toEqual(Error('An unexpected error occured. Could not create item.'))
    })

    it('Adds an image to an existing item', async() => {
        const expectedImage = IMAGE_RESPONSE_DATA

        const response = {
            data: IMAGE_RESPONSE_DATA,
            status: 201,
            statusText: 'Created'
        }
        sinon.stub(axios, 'post').resolves(response)
        const actualImage = await addImageToItem(IMAGE)

        expect(actualImage).toEqual(expectedImage)
    })

    it('Throws error if image could not be added to item', async() => {
        const response = {
            error: 'error'
        }
        sinon.stub(axios, 'post').rejects(response)

        await expect(addImageToItem(ITEM)).rejects.toEqual(Error('An unexpected error occured. Image could not be stored and added to item.'))
    })

    it('Gets all items owned by a user', async() => {
        const expectedImages = [IMAGE_RESPONSE_DATA, IMAGE_RESPONSE_DATA]

        const response = {
            data: [IMAGE_RESPONSE_DATA, IMAGE_RESPONSE_DATA],
            status: 200,
            statusText: 'OK'
        }
        sinon.stub(axios, 'get').resolves(response)
        const actualImages = await getItemImages(ITEM.id)

        expect(actualImages).toEqual(expectedImages)
    })

    it('Throws error if images are not found', async() => {
        const response = {
            error: 'error'
        }
        sinon.stub(axios, 'get').rejects(response)

        await expect(getItemImages(ITEM.id)).rejects.toEqual(Error('An unexpected error occurred. Images to the item could not be loaded.'))
    })

    it('Gets a specified image of an item', async() => {
        const expectedImage = IMAGE_RESPONSE_DATA

        const response = {
            data: IMAGE_RESPONSE_DATA,
            status: 200,
            statusText: 'OK'
        }
        sinon.stub(axios, 'get').resolves(response)
        const actualImage = await getItemImage(ITEM.id, ITEM.imageIds[0])

        expect(actualImage).toEqual(expectedImage)
    })

    it('Throws error if specified item image is not found', async() => {
        const response = {
            error: 'error'
        }
        sinon.stub(axios, 'get').rejects(response)

        await expect(getItemImage(ITEM.id, ITEM.imageIds[0])).rejects.toBeInstanceOf(Error)
    })

    it('Deletes an item specified by id', async() => {
        const expectedItem = ITEM_RESPONSE_DATA

        const response = {
            data: ITEM_RESPONSE_DATA,
            status: 200,
            statusText: 'OK'
        }
        sinon.stub(axios, 'delete').resolves(response)
        const deletedItem = await deleteItem(ITEM.id)

        expect(deletedItem).toEqual(expectedItem)
    })

    it('Throws error if specified item was not deleted', async() => {
        const response = {
            error: 'error'
        }
        sinon.stub(axios, 'delete').rejects(response)

        await expect(deleteItem(ITEM.id)).rejects.toEqual(Error('An unexpected error occured. Item could not be deleted.'))
    })

    it('Gets the category enum values of a generic item', async() => {
        const expectedCategories = CATEGORY_ENUM_RESPONSE_DATA

        const response = {
            data: CATEGORY_ENUM_RESPONSE_DATA,
            status: 200,
            statusText: 'OK'
        }
        sinon.stub(axios, 'get').resolves(response)
        const actualCategories = await getCategories()

        expect(actualCategories).toEqual(expectedCategories)
    })

    it('Throws error if categories could not be retrieved', async() => {
        const response = {
            error: 'error'
        }
        sinon.stub(axios, 'get').rejects(response)

        await expect(getCategories()).rejects.toEqual(Error('An unexpected error occured. Could not get categories.'))
    })

    it('Gets the priceUnit enum values of a generic item', async() => {
        const expectedPriceUnits = PRICEUNIT_ENUM_RESPONSE_DATA

        const response = {
            data: PRICEUNIT_ENUM_RESPONSE_DATA,
            status: 200,
            statusText: 'OK'
        }
        sinon.stub(axios, 'get').resolves(response)
        const actualPriceUnits = await getPriceUnits()

        expect(actualPriceUnits).toEqual(expectedPriceUnits)
    })

    it('Throws error if priceUnits could not be retrieved', async() => {
        const response = {
            error: 'error'
        }
        sinon.stub(axios, 'get').rejects(response)

        await expect(getPriceUnits()).rejects.toEqual(Error('An unexpected error occured. Could not get price units.'))
    })

    it('Gets all items from a search', async() => {
        const expectedItems = [ITEM_RESPONSE_DATA, ITEM_RESPONSE_DATA]

        const response = {
            data: [ITEM_RESPONSE_DATA, ITEM_RESPONSE_DATA],
            status: 200,
            statusText: 'OK'
        }
        sinon.stub(axios, 'get').resolves(response)
        const searchValue = 'Test description'
        const actualItems = await getItems({ search: searchValue })

        expect(actualItems).toEqual(expectedItems)
    })


    it('Updates a specified item', async() => {
        const expectedItem = ITEM_RESPONSE_DATA

        const response = {
            data: ITEM_RESPONSE_DATA,
            status: 200,
            statusText: 'OK'
        }
        sinon.stub(axios, 'put').resolves(response)
        const actualItem = await updateItem(ITEM, ITEM.id)

        expect(actualItem).toEqual(expectedItem)
    })

    it('Throws updateItem error for unchecked statuscode', async() => {
        const error = {
            response: {}
        }
        sinon.stub(axios, 'put').rejects(error)

        await expect(updateItem(ITEM, ITEM.id)).rejects.toEqual(Error('An unexpected error occured. Could not update item.'))
    })
})
