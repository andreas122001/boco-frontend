import { changePassword, deleteUser, getUserdata, register, setUserdata } from '@/services/UserService.js'
import axios from 'axios'

import sinon from 'sinon'

describe('UserService', () => {
    afterEach(() => {
        sinon.restore()
    })

    const USER = {
        firstName: 'Hans',
        lastName: 'Nordmann',
        email: 'hans@text.no',
        streetAddress: 'Testvegen 23A',
        postalCode: 3344,
        password: 'somePass123'
    }

    const USER_RESPONSE_DATA = {
        firstName: 'Hans',
        lastName: 'Nordmann',
        email: 'hans@text.no',
        streetAddress: 'Testvegen 23A',
        postalCode: '3344',
        password: 'somePass123'
    }

    it('Successfully changes password for a user', async() => {
        const oldPassword = 'oldPW'
        const newPassword = 'newPW'

        const response = {
            status: 204,
            statusText: 'No Content'
        }
        sinon.stub(axios, 'put').resolves(response)

        await expect(changePassword(oldPassword, newPassword)).resolves
    })

    it('Throws changePassword error for statuscode 400', async() => {
        const oldPassword = 'oldPW'
        const newPassword = 'newPW'

        const error = {
            response: {
                status: 400,
                statusText: 'Bad Request'
            }
        }
        sinon.stub(axios, 'put').rejects(error)

        const expectedError = 'New password cannot be the same as the old password'

        await expect(changePassword(oldPassword, newPassword)).rejects.toEqual(Error(expectedError))
    })

    it('Throws changePassword error for statuscode 401', async() => {
        const oldPassword = 'oldPW'
        const newPassword = 'newPW'

        const error = {
            response: {
                status: 401,
                statusText: 'Unauthorized'
            }
        }
        sinon.stub(axios, 'put').rejects(error)

        const expectedError = 'Invalid current password'

        await expect(changePassword(oldPassword, newPassword)).rejects.toEqual(Error(expectedError))
    })

    it('Throws changePassword error for unchecked statuscode', async() => {
        const oldPassword = 'oldPW'
        const newPassword = 'newPW'

        const error = {
            response: {}
        }
        sinon.stub(axios, 'put').rejects(error)

        const expectedError = 'An unexpected error occured.'

        await expect(changePassword(oldPassword, newPassword)).rejects.toEqual(Error(expectedError))
    })

    it('Successfully registers users with a unique e-mail', async() => {
        const expectedUser = USER_RESPONSE_DATA

        const response = {
            data: USER_RESPONSE_DATA,
            status: 201,
            statusText: 'Created'
        }
        sinon.stub(axios, 'post').resolves(response)
        const actualUser = await register(USER.email)

        expect(actualUser).toEqual(expectedUser)
    })

    it('Throws register error for statuscode 409', async() => {
        const error = {
            response: {
                status: 409,
                statusText: 'Conflict'
            }
        }
        sinon.stub(axios, 'post').rejects(error)

        const expectedError = 'A user with this e-mail already exists'

        await expect(register(USER_RESPONSE_DATA.email)).rejects.toEqual(Error(expectedError))
    })

    it('Throws register error for unchecked statuscode', async() => {
        const error = {
            response: {}
        }
        sinon.stub(axios, 'post').rejects(error)

        const expectedError = 'An unexpected error occured.'

        await expect(register(USER_RESPONSE_DATA.email)).rejects.toEqual(Error(expectedError))
    })

    it('Successfully retrieves a user profile', async() => {
        const expectedUser = USER_RESPONSE_DATA

        const response = {
            data: USER_RESPONSE_DATA,
            status: 200,
            statusText: 'OK'
        }
        sinon.stub(axios, 'get').resolves(response)
        const actualUser = await getUserdata()

        expect(actualUser).toEqual(expectedUser)
    })

    it('Throws getuserData error for unchecked statuscode', async() => {
        const error = {
            response: {}
        }
        sinon.stub(axios, 'get').rejects(error)

        const expectedError = 'An unexpected error occured. Could not retrieve user profile.'

        await expect(getUserdata()).rejects.toEqual(Error(expectedError))
    })

    it('Successfully updates a user profile', async() => {
        const expectedUser = USER_RESPONSE_DATA

        const response = {
            data: USER_RESPONSE_DATA,
            status: 200,
            statusText: 'OK'
        }
        sinon.stub(axios, 'put').resolves(response)
        const actualUser = await setUserdata(expectedUser)

        expect(actualUser).toEqual(expectedUser)
    })

    it('Throws setuserData error for unchecked statuscode', async() => {
        const error = {
            response: {}
        }
        sinon.stub(axios, 'put').rejects(error)

        const expectedError = 'An unexpected error occured. Could not update user profile.'

        await expect(setUserdata()).rejects.toEqual(Error(expectedError))
    })

    it('Successfully deletes a user profile', async() => {
        const expectedUser = USER_RESPONSE_DATA

        const response = {
            data: USER_RESPONSE_DATA,
            status: 200,
            statusText: 'OK'
        }
        sinon.stub(axios, 'delete').resolves(response)
        const actualUser = await deleteUser()

        expect(actualUser).toEqual(expectedUser)
    })

    it('Throws delete error for unchecked statuscode', async() => {
        const error = {
            response: {}
        }
        sinon.stub(axios, 'delete').rejects(error)

        const expectedError = 'An unexpected error occured. Could not delete user profile.'

        await expect(deleteUser()).rejects.toEqual(Error(expectedError))
    })
})
