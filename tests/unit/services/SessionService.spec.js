import { createSession, deleteSession, InvalidCredentialsError } from '@/services/SessionService.js'
import axios from 'axios'

import sinon from 'sinon'

describe('SessionService', () => {
    afterEach(() => {
        sinon.restore()
    })

    it('Creates session if logged in successfully', async() => {
        const expectedToken = 'bdakbdahjdvahodavk'

        const response = {
            data: { token: expectedToken },
            status: 201,
            statusText: 'Created'
        }
        sinon.stub(axios, 'post').resolves(response)

        const userCredentials = { email: 'mc@hammer.com', password: 'HammerTime' }
        const session = await createSession(userCredentials)

        expect(session.token).toEqual(expectedToken)
    })

    it('Throws InvalidCredentialsError for statuscode 401', async() => {
        const error = {
            response: {
                status: 401,
                statusText: 'Unauthorized'
            }
        }
        sinon.stub(axios, 'post').rejects(error)

        const userCredentials = { email: 'mc@hammer.com', password: 'HammerTime' }
        await expect(createSession(userCredentials)).rejects.toThrow(InvalidCredentialsError)
    })

    it('Throws createSession error for unchecked statuscode', async() => {
        const error = {
            response: {}
        }
        sinon.stub(axios, 'post').rejects(error)

        const userCredentials = { email: 'mc@hammer.com', password: 'HammerTime' }
        await expect(createSession(userCredentials)).rejects.toEqual(Error('An unexpected error occured.'))
    })

    it('Returns nothing if session was deleted successfully', async() => {
        const response = {
            status: 201,
            statusText: 'OK',
            data: undefined
        }
        sinon.stub(axios, 'delete').resolves(response)

        const session = { token: 'bdakbdahjdvahodavk' }

        await expect(deleteSession(session)).resolves.toBeUndefined()
    })

    it('Throws error if session was not properly deleted', async() => {
        const error = {
            response: {}
        }
        sinon.stub(axios, 'delete').rejects(error)

        const session = { token: 'bdakbdahjdvahodavk' }
        await expect(deleteSession(session)).rejects.toEqual(Error('An unexpected error occured.'))
    })
})
