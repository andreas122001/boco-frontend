# boco-frontend

## Project
This is the front end repository for group 3 in the SCRUM-project in the subject IDATT2106 at NTNU. The project took place from 19.04.2022 to 16.05.2022.

Here you can find the code used in conjunction with the [back end](https://gitlab.stud.idi.ntnu.no/idatt2106-v21-03/boco-backend) to serve up the BOCO service.

## BOCO
BOCO is a fictional business concept where users may rent out items they own and/or rent others' items on a public marketplace.

The service provides a way for users to create, edit and delete items for rental, rent others' items and message others users.

To learn more about BOCO, please refer to the [vision document](https://idatt2106-v21-03.pages.stud.idi.ntnu.no/vision-document/).

## Additional information
More information about the project in its entirety may be found on our [Wiki](https://gitlab.stud.idi.ntnu.no/idatt2106-v21-03/boco-backend/-/wikis/home).

## Project setup
To use the application you must first have the [back end](https://gitlab.stud.idi.ntnu.no/idatt2106-v21-03/boco-backend) up and running. This can be hosted on your local machine for testing, or served from a domain of your choosing.

Next, you will need a local `.env` file in the root folder of the project. This is done to prevent accidentally sharing crucial, private business information such as API keys. Your environment file should like the following:

```
VUE_APP_API_ENDPOINT=
VUE_APP_GOOGLE_MAPS_API_KEY=
```

Where `VUE_APP_API_ENDPOINT` is the root of your backend domain (e.g. `http://localhost:8080`) and `VUE_APP_GOOGLE_MAPS_API_KEY` is your API key for using Google Maps embeds.

Next up, you must run the following command in a terminal in the root folder of the project to install the necessary dependencies.

```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
