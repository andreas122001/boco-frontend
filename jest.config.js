module.exports = {
    preset: '@vue/cli-plugin-unit-jest',
    collectCoverage: true,
    collectCoverageFrom: ['src/**/*.{js,vue}', '!src/main.js', '!src/**/index.js', '!src/plugins/*'],
    coverageReporters: ['html', 'text', 'text-summary', 'cobertura'],
    coverageThreshold: {
        global: {
            lines: 30
        }
    }
}
